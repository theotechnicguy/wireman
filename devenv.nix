{ pkgs, lib, config, inputs, ... }:

{
	name = "WireMan";

  # https://devenv.sh/packages/
  packages = with pkgs; [
		git
		gitleaks
	];

  # https://devenv.sh/languages/
	# The server is written in Go
  languages.go.enable = true;

	# Pre-commit development hooks
  # https://devenv.sh/pre-commit-hooks/
	pre-commit.hooks = {
		shellcheck.enable = true;
		end-of-file-fixer.enable = true;
		check-added-large-files.enable = true;
		gitleaks = {
			enable = true;
			name = "gitleaks";
			description = "Check for secrets in the source code";
			entry = "${pkgs.gitleaks}/bin/gitleaks detect";
		};
		gofmt.enable = true;
	};

  # See full reference at https://devenv.sh/reference/options/
}
