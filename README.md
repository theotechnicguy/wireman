# WireMan - A WireGuard Manager

The idea for WireMan is to allow users easy and secure self-servicing for [WireGuard](https://www.wireguard.com) VPN peers. Instead of having to change the server's configuration for every new device, users can create, update, and delete their peers on their own without administrative intervention. Whatever WireGuard server you use, present your users with a unified and simple interface.

<!-- TODO: Screenshots -->

## :warning: Alpha development

This project is at it's baby stages. Expect bugs and breakage.

## How WireMan is structured

WireMan has different layers so that it can support many different WireGuard servers.

- The user interface, either the web-based GUI or the CLI
- The WireMan server that serves a unified API and manages permissions
- The connector that translates the WireGuard server's API to the WireMan API

<!--
TODO: installation guide
TODO: Roadmap link
TODO: Contribution guide / dev requirements
    - [ ] Software stack used
    - [ ] Versioning guide
    - [ ] CONTRIBUTING.md
-->

## Legal

This project is licensed under the terms in the [license](LICENSE).
